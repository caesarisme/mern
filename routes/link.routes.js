const { Router } = require('express')
const router = Router()
const config = require('config')
const shortid = require('shortid')

const auth = require('../middleware/auth.middleware')

const Link = require('../models/Link')


router.get('/', auth, async (req, res) => {
  try {
    const links = await Link.find({ owner: req.user.userId })
    res.status(200).json(links)
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})

router.post('/generate', auth, async (req, res) => {
  try {
    const baseUrl = config.get('BASE_URL')
    const { from } = req.body
    const code = shortid.generate()

    const existing = await Link.findOne({ from })
    if (existing) {
      return res.status(200).json({ link: existing })
    }

    const to = baseUrl + '/t/' + code

    const newLink = new Link({ code, to, from, owner: req.user.userId})
    await newLink.save()

    res.status(201).json({ link: newLink })

  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})

router.get('/:id', auth, async (req, res) => {
  try {
    const link = await Link.findById(req.params.id)
    res.status(200).json(link)
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})


module.exports = router